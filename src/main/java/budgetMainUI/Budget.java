package budgetMainUI;

import entities.Record;
import entities.table.TableController;
import menu.Menu;
import menu.ConsoleHelper;

import java.util.Date;

public class Budget {
	private Menu menu;
	private TableController incomes;
	private TableController expenses;

	public static void main(String[] args) {
		Budget budget = new Budget();
		// -----------------
		budget.createExampleTables();
		// -----------------
		ConsoleHelper.printGreetings();
		budget.menu.enterMainMenu();
	}
	
	public Budget() {
		menu = new Menu(this);
		incomes = new TableController();
		expenses = new TableController();
	}
	
	private void createExampleTables() {
		Record[] incomeList = {new Record("Salary", 1000, new Date()), new Record("Scholarship", 200, new Date()),
				new Record("Pension", 300, new Date()), new Record("Present", 100, new Date()),
				new Record("Other", 1000, new Date())};
		
		Record[] expensesList = {new Record("Grocery store", 800, new Date()), new Record("Leasing", 300, new Date()),
				new Record("Communal payments", 700, new Date()), new Record("Studies", 500, new Date()),
				new Record("Entertainment", 200, new Date()), new Record("Auto", 100, new Date()),
				new Record("Clothes", 200, new Date()), new Record("Health", 100, new Date()),
				new Record("Other", 50, new Date())};
		
		for (Record record : incomeList) {
			incomes.addRecord(record);
		}
		for (Record record : expensesList) {
			expenses.addRecord(record);
		}
	}
	
	public TableController getIncomes() {
		return incomes;
	}
	
	public TableController getExpenses() {
		return expenses;
	}
}


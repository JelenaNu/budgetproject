package menu;

import budgetMainUI.Budget;
import entities.table.TableController;

public class Menu {
	private Budget budget;
	
	public Menu(Budget budget) {
		this.budget = budget;
	}
	
	private final String[] MAIN_MENU = new String[]{
			"Select an action: ",
			"\"I\" - explore INCOME",
			"\"E\" - explore EXPENSES",
			"\"B\" - show BALANCE",
			"\"X\" - CLOSE"
	};
	
	private final String[] SUB_MENU = new String[]{
			"Select an action: ",
			"\"A\" - ADD a record",
			"\"R\" - REMOVE a record",
			"\"E\" - EDIT a record",
			"\"S\" - SHOW table",
			"\"B\" - BACK"
	};
	
	public void enterSubMenu(TableController controller) {
		
		final String AVAILABLE_COMMANDS = "ADRSBE";
		
		while (true) {
			showMenu(SUB_MENU);
			char command = ConsoleHelper.getCommandFromUser(AVAILABLE_COMMANDS);
			if (command == 'A') {
				MenuCommandHelper.addRecord(controller);
			} else if (command == 'R') {
				MenuCommandHelper.removeRecord(controller);
			} else if(command == 'E') {
				MenuCommandHelper.editRecord(controller);
			} else if (command == 'S') {
				MenuCommandHelper.showTable(controller);
			} else if (command == 'B') {
				ConsoleHelper.printLine("> back to main...");
				break;
			} else {
				ConsoleHelper.printWrongCommand();
			}
		}
	}
	
	public void enterExpensesMenu() {
		ConsoleHelper.printLine("> enter expenses menu...");
		enterSubMenu(budget.getExpenses());
	}
	
	public void enterIncomesMenu() {
		ConsoleHelper.printLine("> enter incomes menu..");
		enterSubMenu(budget.getIncomes());
	}
	
	public void enterMainMenu() {
		
		final String AVAILABLE_COMMANDS = "IEBX";
		
		while (true) {
			showMenu(MAIN_MENU);
			char command = ConsoleHelper.getCommandFromUser(AVAILABLE_COMMANDS);
			switch (command) {
				case 'I':
					enterIncomesMenu();
					break;
				case 'E':
					enterExpensesMenu();
					break;
				case 'B':
					MenuCommandHelper.showBalance(budget.getIncomes().getTotal(), budget.getExpenses().getTotal());
					break;
				case 'X':
					System.exit(0);
				default:
					ConsoleHelper.printWrongCommand();
			}
		}
	}
	
	public void showMenu(String[] menuList) {
		for (String s: menuList) {
			ConsoleHelper.printLine(s);
		}
	}
}

package menu;

import entities.Record;
import entities.table.TableController;

public class MenuCommandHelper {
	
	public static void editRecord(TableController controller) {
		showTable(controller);
		
		ConsoleHelper.printLine(" > edit a record... Select a line number:");
		int lineNumber = ConsoleHelper.getNumberFromUser();
		
		if (lineNumber > 0 && lineNumber <= controller.getRecords().length) {
			
			ConsoleHelper.printLine(" > edit a description... Write a description: (press enter without data to leave the same)");
			String description = ConsoleHelper.getStringFromUser();
			ConsoleHelper.printLine("Write an amount: (write -1 to leave the same)");
			double amount = ConsoleHelper.getAmmountFromUser();
			controller.editRecord(lineNumber - 1, description, amount);
			ConsoleHelper.printLine("The record was successfully changed!");
			
			showTable(controller);
			
		} else {
			ConsoleHelper.printLine("Wrong record number");
		}
	}
	
	public static void addRecord(TableController controller) {
		ConsoleHelper.printLine("> add a record...");
		ConsoleHelper.printLine("Write a description: ");
		String description = ConsoleHelper.getStringFromUser();
		
		ConsoleHelper.printLine("Write an amount: ");
		double amount = ConsoleHelper.getAmmountFromUser();
		if (amount > -1) {
			controller.addRecord(new Record(description, amount));
			ConsoleHelper.printLine("The record was successfully added!");
		}
		
		showTable(controller);
	}
	
	public static void removeRecord(TableController controller) {
		showTable(controller);
		
		ConsoleHelper.printLine("> remove a record... Select a line number:");
		int number = ConsoleHelper.getNumberFromUser();
		if (!controller.removeRecord(number - 1)) {
			ConsoleHelper.printLine("Wrong record number");
		} else {
			ConsoleHelper.printLine("The record was successfully deleted!");
		}
		
		showTable(controller);
	}
	
	public static void showTable(TableController controller) {
		ConsoleHelper.printLine(" > show table...");
		ConsoleHelper.printDelimiter();
		
		double total = 0;
		for (int i = 0; i < controller.getRecords().length; i++) {
			if (controller.getRecords()[i] != null) {
				ConsoleHelper.print(String.format("| %d. ", i + 1));
				ConsoleHelper.print(String.format("| %-20s", controller.getRecords()[i].getDescription()));
				ConsoleHelper.print(String.format("| %1$tY.%1$tm.%1$td ", controller.getRecords()[i].getDate()));
				ConsoleHelper.print(String.format("| %10.2f |\n", controller.getRecords()[i].getAmount()));
				ConsoleHelper.printDelimiter();
				
				total += controller.getRecords()[i].getAmount();
			}
		}
		
		ConsoleHelper.print(String.format("Total: %5.2f %n%n", total));
	}
	
	public static void showBalance(double incomesTotal, double expensesTotal) {
		ConsoleHelper.printLine("> show balance...");
		ConsoleHelper.print(String.format(" TOTAL:\n\tIncomes: %.2f\n\tExpenses: %.2f\n\tBalance: %.2f\n\n", incomesTotal,
				expensesTotal, incomesTotal - expensesTotal));
	}
	
}

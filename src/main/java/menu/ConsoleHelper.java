package menu;

import java.util.Scanner;

public class ConsoleHelper {
	
	private static Scanner scanner;
	
	static {
		scanner = new Scanner(System.in);
	}
	
	public static void printLine(String string) {
		System.out.println(string);
	}
	
	public static void print(String string) {
		System.out.print(string);
	}
	
	public static String getStringFromUser() {
		return scanner.nextLine();
	}
	
	public static int getNumberFromUser() {
		int lineNumber = 0;
		while (true) {
			String lineNumberString = getStringFromUser();
			if (lineNumberString.equalsIgnoreCase("X")) {
				printLine("> cancel...");
				break;
			}
			try {
				lineNumber = Integer.parseInt(lineNumberString);
				break;
			} catch (Exception e) {
				printLine("Wrong line number. Try again or press \"X\" to cancel");
			}
		}
		return lineNumber;
	}
	
	public static void printWrongCommand() {
		System.err.println("Wrong command");
	}
	
	public static double getAmmountFromUser() {
		double amount = -1;
		while (true) {
			String amountString = getStringFromUser();
			if (amountString.equalsIgnoreCase("X")) {
				printLine("> cancel...");
				break;
			}
			try {
				amount = Double.parseDouble(amountString);
				break;
			} catch (Exception e) {
				printLine("Wrong amount. Try again or press \"X\" to cancel");
			}
		}
		return amount;
	}
	
	public static char getCommandFromUser(String availableCommands) {
		String command;
		while (true) {
			command = getStringFromUser().toUpperCase();
			if (command.length() == 1 && availableCommands.contains(command)) {
				return command.charAt(0);
			} else {
				printWrongCommand();
			}
		}
	}
	
	public static void printGreetings() {
		ConsoleHelper.printLine("\n*** Welcome to budgetUI.Budget Project ***\n");
	}
	
	public static void printDelimiter() {
		System.out.println("------------------------------------------------------");
	}
	
	private ConsoleHelper() {
	}
}

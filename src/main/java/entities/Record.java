package entities;

import java.util.Date;

public class Record {
	private String description;
	private double amount;
	private Date date;
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public Record(String description, double amount, Date date) {
		this.description = description;
		this.amount = amount;
		this.date = date;
	}

	public Date getDate() {
		return date;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Record(String description, double amount) {
		this.description = description;
		this.amount = amount;
		this.date = new Date();
	}
}

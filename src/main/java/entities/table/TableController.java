package entities.table;

import entities.Record;

public class TableController {
	private Table table;
	
	public TableController() {
		this.table = new Table();
	}
	
	public double getTotal() {
		double total = 0;
		for (Record record : table.getRecordList()) {
			total += record.getAmount();
		}
		return total;
	}
	
	public Record[] getRecords() {
		return table.getRecordList();
	}
	
	public void addRecord(Record record) {
		Record[] newArray = new Record[table.getRecordList().length + 1];
		System.arraycopy(table.getRecordList(), 0, newArray, 0, table.getRecordList().length);
		newArray[newArray.length - 1] = record;
		table.setRecordList(newArray);
	}
	
	public boolean removeRecord(int recordIndex) {
		if (0 <= recordIndex && recordIndex < table.getRecordList().length) {
			Record[] newArray = new Record[table.getRecordList().length - 1];
			System.arraycopy(table.getRecordList(), 0, newArray, 0, recordIndex);
			System.arraycopy(table.getRecordList(), recordIndex + 1, newArray, recordIndex,
					table.getRecordList().length - recordIndex - 1);
			table.setRecordList(newArray);
			return true;
		} else {
			return false;
		}
	}
	
	public boolean editRecord(int recordIndex, String newDescription, double newAmount) {
		Record record = table.getRecordList()[recordIndex];
		boolean changed = false;
		if (newAmount > -1) {
			record.setAmount(newAmount);
			changed = true;
		}
		if (!newDescription.equals("")) {
			record.setDescription(newDescription);
			changed = true;
		}
		return changed;
	}
}

package entities.table;

import entities.Record;

public class Table {
	
	private Record[] recordList;
	
	public Table() {
		recordList = new Record[0];
	}
	
	public Record[] getRecordList() {
		return recordList;
	}

	public void setRecordList(Record[] recordList) {
		this.recordList = recordList;
	}
}


package entities;

import java.util.Arrays;

public class Table {
	
	private Record[] recordList;
	
	public double getTotal() {
		double total = 0;
		for (Record record : recordList) {
			total += record.getAmount();
		}
		return total;
	}
	
	public void addRecord(Record record) {
		Record[] newArray = new Record[recordList.length + 1];
		System.arraycopy(recordList, 0, newArray, 0, recordList.length);
		newArray[newArray.length - 1] = record;
		recordList = newArray;
	}
	
	public boolean removeRecord(int recordIndex) {
		if (0 <= recordIndex && recordIndex < recordList.length) {
			Record[] newArray = new Record[recordList.length - 1];
			System.arraycopy(recordList, 0, newArray, 0, recordIndex);
			System.out.println(Arrays.toString(newArray));
			System.out.println(recordList[recordIndex + 1]);
			System.out.println(recordList.length - recordIndex);
			System.arraycopy(recordList, recordIndex + 1, newArray, recordIndex, recordList.length - recordIndex - 1);
			recordList = newArray;
			return true;
		} else {
			return false;
		}
	}
	public boolean editRecord(int recordIndex, String newDescription, double newAmount) {
		Record record = recordList[recordIndex];
		boolean changed = false;
		if (newAmount > -1) {
			record.setAmount(newAmount);
			changed = true;
		}
		if (!newDescription.equals("")) {
			record.setDescription(newDescription);
			changed = true;
		}
		return changed;
	}
	
	public Table() {
		recordList = new Record[0];
	}
	
	public Record[] getRecordList() {
		return recordList;
	}
	
	/*@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		for (Record record : recordList) {
			if (record == null) {
				break;
			} else result.append(record.toString()).append("\n");
		}
		return result.toString();
	}*/
	
}


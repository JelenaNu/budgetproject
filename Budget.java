import entities.Record;
import entities.Table;

import java.util.Date;
import java.util.Scanner;

public class Budget {
	
	private static Table incomes;
	private static Table expenses;
	private static Scanner scanner;
	
	private static String[] mainMenu = new String[]{
			"Select an action: ",
			"\"I\" - explore INCOME",
			"\"E\" - explore EXPENSES",
			"\"B\" - show BALANCE",
			"\"X\" - CLOSE"
	};
	
	private static String[] subMenu = new String[]{
			"Select an action: ",
			"\"A\" - ADD a record",
			"\"R\" - REMOVE a record",
			"\"E\" - EDIT a record",
			"\"S\" - SHOW table",
			"\"B\" - BACK"
	};

	public static void main(String[] args) {
		initialize();
		// -----------------
		createExampleTables();
		// -----------------
		printGreetings();
		enterMainMenu();
	}
	
	private static void initialize() {
		scanner = new Scanner(System.in);
		incomes = new Table();
		expenses = new Table();
	}
	
	private static void createExampleTables() {
		Record[] incomeList = {new Record("Salary", 1000, new Date()), new Record("Scholarship", 200, new Date()),
				new Record("Pension", 300, new Date()), new Record("Present", 100, new Date()),
				new Record("Other", 1000, new Date())};
		
		Record[] expensesList = {new Record("Grocery store", 800, new Date()), new Record("Leasing", 300, new Date()),
				new Record("Communal payments", 700, new Date()), new Record("Studies", 500, new Date()),
				new Record("Entertainment", 200, new Date()), new Record("Auto", 100, new Date()),
				new Record("Clothes", 200, new Date()), new Record("Health", 100, new Date()),
				new Record("Other", 50, new Date())};
		for (Record record : incomeList) {
			incomes.addRecord(record);
		}
		for (Record record : expensesList) {
			expenses.addRecord(record);
		}
	}
	
	static void print(String string) {
		System.out.println(string);
	}
	
	private static void enterSubMenu(Table table) {
		String availableCommands = "ADRSBE";
		while (true) {
			printMenu(subMenu);
			char command = getConsoleCommand(availableCommands);
			if (command == 'A') {
				addRecord(table);
			} else if (command == 'R') {
				removeRecord(table);
			} else if(command == 'E') {
				editRecord(table);
			} else if (command == 'S') {
				printTable(table);
			} else if (command == 'B') {
				print("> back to main...");
				break;
			} else {
				wrongCommand();
			}
		}
	}
	private static int getConsoleLineNumber() {
		int lineNumber = 0;
		while (true) {
			String lineNumberString = scanner.nextLine();
			if (lineNumberString.equalsIgnoreCase("X")) {
				print("> cancel...");
				break;
			}
			try {
				lineNumber = Integer.parseInt(lineNumberString);
				break;
			} catch (Exception e) {
				print("Wrong line number. Try again or press \"X\" to cancel");
			}
		}
		return lineNumber;
	}
	
	private static void editRecord(Table table) {
		printTable(table);
		print(" > edit a record... Select a line number:");
		int lineNumber = getConsoleLineNumber();
		
		if (lineNumber > 0 && lineNumber <= table.getRecordList().length) {
			print(" > edit description... Write description: (press enter without data to leave the same)");
			String description = scanner.nextLine();
			print("Write amount: (write -1 to leave the same)");
			double amount = getConsoleAmount();
			table.editRecord(lineNumber - 1, description, amount);
			print("Record successfully changed!");
			printTable(table);
		} else {
			print("Wrong record number");
		}
	}
	
	private static void wrongCommand() {
		print("Wrong command");
	}
	
	private static double getConsoleAmount() {
		double amount = -1;
		while (true) {
			String amountString = scanner.nextLine();
			if (amountString.equalsIgnoreCase("X")) {
				print("> cancel...");
				break;
			}
			try {
				amount = Double.parseDouble(amountString);
				break;
			} catch (Exception e) {
				print("Wrong amount. Try again or press \"X\" to cancel");
			}
		}
		return amount;
	}
	private static void addRecord(Table table) {
		print("> add a record...");
		print("Write description: ");
		String description = scanner.nextLine();
		print("Write amount: ");
		double amount = getConsoleAmount();
		if (amount > -1) {
			table.addRecord(new Record(description, amount));
			print("Record successfully added!");
		}
		printTable(table);
	}
	
	private static void enterExpensesMenu() {
		enterSubMenu(expenses);
	}
	
	private static void enterIncomesMenu() {
		enterSubMenu(incomes);
	}
	
	private static void removeRecord(Table table) {
		print("> remove a record... Select a line number:");
		int number = scanner.nextInt();
		if (!table.removeRecord(number - 1)) {
			print("Wrong record number");
		} else {
			print("Record successfully deleted!");
		}
		printTable(table);
	}
	
	private static void enterMainMenu() {
		String availableCommands = "IEBX";
		
		while (true) {
			printMenu(mainMenu);
			char command = getConsoleCommand(availableCommands);
			switch (command) {
				case 'I':
					print("> enter incomes menu..");
					enterIncomesMenu();
					break;
				case 'E':
					print("> enter expenses menu...");
					enterExpensesMenu();
					break;
				case 'B':
					print("> show balance...");
					showBalance();
					break;
				case 'X':
					System.exit(0);
				default:
					wrongCommand();
			}
		}
	}
	
	private static void showBalance() {
		System.out.printf(" TOTAL:\n\tIncomes: %.2f\n\tExpenses: %.2f\n\tBalance: %.2f\n\n", incomes.getTotal(),
				expenses.getTotal(), incomes.getTotal() - expenses.getTotal());
	}
	
	private static char getConsoleCommand(String availableCommands) {
		String command;
		while (true) {
			command = scanner.nextLine().toUpperCase();
			if (command.length() == 1 && availableCommands.contains(command)) {
				return command.charAt(0);
			} else {
				wrongCommand();
			}
		}
	}
	
	private static void printGreetings() {
		print("\n*** Welcome to Budget Project ***\n");
	}
	private static void printMenu(String[] menuList) {
		for (String s: menuList) {
			print(s);
		}
	}
	
	static void printTable(Table table) {
		print(" > show table...");
		print("----------------------------------------------------");
		double total = 0;
		for (int i = 0; i < table.getRecordList().length; i++) {
			if (table.getRecordList()[i] != null) {
				System.out.printf("| %d. ", i + 1);
				System.out.printf("| %-20s", table.getRecordList()[i].getDescription());
				System.out.printf("| %1$tY.%1$tm.%1$td ", table.getRecordList()[i].getDate());
				System.out.printf("| %10.2f %n", table.getRecordList()[i].getAmount());
				System.out.println("----------------------------------------------------");
				
				total += table.getRecordList()[i].getAmount();
			}
		}
		System.out.printf("Total: %5.2f %n%n", total);
	}
	
}

